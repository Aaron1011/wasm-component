//! This crate contains the basic tools to create a Rust-WebAssembly application
//! with a logic of `Driver` and `Finite State Machine`.
//! Each Driver is associated to StateMachine in an App
//! and the App handles the communication between the Graphic Context and the Drivers.
//! > (why are you asking yourselve, haven't he done it on each Drivers are you saying ?
//! That's because I don't want to have X closures to forget on the Rust side, so I am coercing all
//! the lifetimes into the App and forgetting only 1 closure, the App's one. There surely are
//! better solutions but for the moment i don't know :) )
pub extern crate nom_html_parser;
pub extern crate wasm_bindgen;
pub extern crate mustache;
pub extern crate linked_hash_map;
pub extern crate uuid;
pub extern crate gloo;
pub extern crate web_sys;
pub extern crate js_sys;

extern crate serde;
extern crate serde_derive;
extern crate serde_json;

pub mod state_machine;

pub mod v2;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
