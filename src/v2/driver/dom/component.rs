use crate::v2::driver::dom::AppEvent;

pub trait DOMComponent: Sized + 'static {
   fn create() -> Self;

   fn receive(&mut self, evt: AppEvent) -> Result<(), String>;

   fn get_data(&mut self) -> mustache::MapBuilder;
   fn get_childrens(&mut self) -> mustache::MapBuilder;

   fn view(&self) -> &'static str;
   fn style(&self) -> &'static str;

   fn render_child(&mut self, element_id: String, tag: String, parent: Option<web_sys::Element>) -> Result<(), ()> {
      unimplemented!()
   }
   fn destroy_child(&mut self, element_id: String) {
      unimplemented!()
   }
}
